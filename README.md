# Lightyear public CSS & JS code #

### What is this repository for? ###

This repository is used to develop and serve public JS and CSS custom code for Webflow Lightyear website.

### How do I get set up? ###

* Config & use
* Development use
* Production use


## Config & use ##
Clone `git clone git@bitbucket.org:koen_lighyear/webflow-website-public.git`

Add or make changes as desired. Stage changes:
`git commit . -m "MESSAGE WITH UPDATES DESCRIPTION"`

Push to save and deploy:
`git push origin master`

## Development ##
`https://bb.githack.com/koen_lighyear/webflow-website-public/raw/master/custom.css`
`https://bb.githack.com/koen_lighyear/webflow-website-public/raw/master/custom.js`

## Production ##
`https://bbcdn.githack.com/koen_lighyear/webflow-website-public/raw/master/custom.css`
`https://bbcdn.githack.com/koen_lighyear/webflow-website-public/raw/master/custom.js`

### Who do I talk to? ###

Koen