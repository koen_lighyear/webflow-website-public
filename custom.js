
$(document).ready(function(e) {
  
  // inline form code
    $(".form-inline").focusin(function() {
        $(this).find(".field-inline-start").removeClass("field-inline-nofocus");
    });
    $(".form-inline").focusout(function() {
        $(this).find(".field-inline-start").addClass("field-inline-nofocus");
    });

    // manual function for checking current breakpoint
    var isBreakPoint = function(bp) {
        var breakpoints = [479, 768, 992, 1280, 1440, 1920, 10000], w = $(window).width(), min, max
        for (var i = 0, l = breakpoints.length; i < l; i++) {
            if (breakpoints[i] === bp) {
                min = breakpoints[i - 1] || 0
                max = breakpoints[i]
                break
            }
        }
        return w > min && w <= max
    }
    
    // show link of page that is currently visible
    $("[href]").each(function() {
        if (this.href == window.location.href) {
            $(this).addClass("current");
        }
    });
    

    // nav bar animation on should be triggered on scroll
$(window).scroll(animateNavBar);

//nav bar move on scroll animation variables
var menuActive = false;
// check if the side menu is open or not
var previousScrollPos = 0;
var isShown = true;

// hide the nav bar if a page is loaded is not starting at top of page
if ($(this).scrollTop() > $(".nav-bar").data("height-large")) {
  // $(".nav-bar").css({
  //   top: "-" + $(".nav-bar").data("height-large") + "px"
  // });
  $(".nav-bar").addClass("nav-bar-hidden");
  isShown = false;
}

// function to move nav bar based on scroll 
function animateNavBar() {
  var currentScrollPos = $(this).scrollTop();
  var navBarHeightLarge = $(".nav-bar").data("height-large");
  var navBarHeightSmall = $(".nav-bar").data("height-small");

  //at the top of the page the nav bar is always visible with large height
  if (currentScrollPos < (navBarHeightLarge - navBarHeightSmall)) {
    // $(".nav-bar").innerHeight(navBarHeightLarge - currentScrollPos);
    // $(".nav-bar-background").stop(true).css("opacity", "0");
    $(".nav-bar").removeClass("nav-bar-not-pagetop");
    //console.log("top of screen nav bar");
  } else {
    if (!menuActive) {
      // move nav bar out of screen
      if (currentScrollPos > previousScrollPos && isShown) {
        $(".nav-bar").addClass("nav-bar-hidden");
        $(".nav-bar").removeClass("nav-bar-not-pagetop");
        // $(".nav-bar").animate({
        //   top: "-" + navBarHeightLarge + "px"
        // }, 200);
        isShown = false;
      } else {
        // move nav bar back into screen with white background and smaller height
        if (currentScrollPos < previousScrollPos && !isShown) {
          $(".nav-bar").removeClass("nav-bar-hidden");
          $(".nav-bar").addClass("nav-bar-not-pagetop");
          // $(".nav-bar-background").stop(true).css({
          //   "background-color": "#fff",
          //   "opacity": "1"
          // });
          // $(".nav-bar, .nav-bar-menu-button, .lightyear-logo").css({
          //   "color": "black"
          // })
          // $(".nav-bar").animate({
          //   top: "0px"
          // }, 300).height(navBarHeightSmall);
          isShown = true;
        }
      }
    }
  }
  previousScrollPos = currentScrollPos;
}

    // hide menu once at start
    $(".main-menu > .main-menu-list-item").fadeTo(200, 0).animate({
        left: '3%'
    }, 0);
    $(".secondary-menu-list-item").fadeTo(200, 0).animate({
        left: '1%'
    }, 0);
    // .css("transform","translate(200px, 0px)");
    $(".menu-container-grid").css({
        "transform": "translate(100%, 0px)",
        "transition": "0.4s ease-in-out all"
    });
    $(".menu-background").fadeOut(500, function() {
        $(".menu-container").hide();
    });

    // menu open-close animation
    $(".menu-button, .menu-grid").click(function() {
        console.log("clicked");
        var el = $("#menu-button");
        var currentScrollPos = $(window).scrollTop();
        var navBarHeightLarge = $(".nav-bar").data("height-large");

        if (!menuActive) {
            $("body").css({"overflow": "hidden"});
            $(".menu-container").show();
              $(".nav-bar").addClass("nav-bar-hidden-menu-open");
            $(".nav-bar-menu-list").fadeOut(300);
            $(".menu-background").fadeTo(500, 0.8);
            $(".menu-container-grid").css({
                "transform": "translate(0%, 0px)",
                  "transition": "0.4s cubic-bezier(0.65, 0, 0.35, 1) all"
                // "transition": "0.4s ease-in-out all"
            });
            $(".main-menu > .main-menu-list-item").delay(50).each(function(i) {
                $(this).delay(100 * i).animate({
                    left: '0%',
                    opacity: '1',
                }, 400);
            });
            $(".secondary-menu-list-item").delay(800).each(function(i) {
                $(this).delay(100 * i).animate({
                    left: '0%',
                    opacity: '1'
                }, 200);
            });
            el.fadeOut(200, function() {
                el.text(el.data("text-swap"));
            });
            el.fadeIn(500);
            menuActive = true;
        } else {
            // hide menu
              $("body").css({"overflow": "visible"});
          
            $(".main-menu > .main-menu-list-item").fadeTo(200, 0).animate({
                left: '3%'
            }, 0);
            $(".secondary-menu-list-item").fadeTo(200, 0).animate({
                left: '1%'
            }, 0);
            // .css("transform","translate(200px, 0px)");
            $(".menu-container-grid").css({
                "transform": "translate(100%, 0px)",
                "transition": "0.4s ease-in-out all"
            });
              $(".nav-bar-menu-list").delay(300).fadeIn(300);
            $(".menu-background").fadeOut(500, function() {
                $(".menu-container").hide();
            });
          

            // only show nav-bar background when not at top of page
            if (currentScrollPos > navBarHeightLarge) {
                  $(".nav-bar").removeClass("nav-bar-hidden-menu-open");
            }

            // swap "close" for "menu"
            el.fadeOut(200, function() {
                el.text(el.data("text-original"));
            });
            el.fadeIn(500);

            menuActive = false;

        }
    });

    // all code for image sliders

    // cursor code (not finished)
    var $cursorLeft = $('.cursor-left');
    var $cursorRight = $('.cursor-right');
    var cursorRadius = $cursorRight.outerWidth() * 0.5;
    

    $("body").on({ 
        mouseover: function() {
            $cursorRight.removeClass("cursor-hidden");
            $cursorRight.addClass("cursor-show");
        },
        mouseout: function() {
            $cursorRight.removeClass("cursor-show");
            $cursorRight.addClass("cursor-hidden");
        }
    }, ".slider-selector-right");

    $("body").on({
        mouseover: function() {
            $cursorLeft.removeClass("cursor-hidden");
            $cursorLeft.addClass("cursor-show");
        },
        mouseout: function() {
            $cursorLeft.removeClass("cursor-show");
            $cursorLeft.addClass("cursor-hidden");
        }
    }, ".slider-selector-left");

    $(document).mousemove(function(e) {
        var cursorY = e.pageY - cursorRadius;
        var cursorX = e.pageX - cursorRadius;
        $cursorRight.css({
            "top": cursorY + "px",
            "left": cursorX + "px",
        })

        $cursorLeft.css({
            "top": cursorY + "px",
            "left": cursorX + "px",
        })
    })
    
    
    $(".slider-selector-left").hide();
    $(".slider-selector-left").click(function(e) {
        var $sliderWrapper = $(this).parent();
        var $slider = $sliderWrapper.find(".slider");  
        var sliderTarget = $slider.data('current-slider-index') - 1;
        moveSlider($slider, $sliderWrapper, sliderTarget);
    });

    $(".slider-selector-right").click(function(e) {        
        var $sliderWrapper = $(this).parent();
        var $slider = $sliderWrapper.find(".slider");
        var sliderTarget = $slider.data('current-slider-index') + 1;
        moveSlider($slider, $sliderWrapper, sliderTarget);
    });

    function moveSlider($slider, $sliderWrapper, sliderTarget){
        var width = $slider.children().first().width();
        $slider.data('current-slider-index', sliderTarget);
        console.log(sliderTarget + " - " + $slider.children().length);
        if(sliderTarget >= $slider.children().length-2){
            $sliderWrapper.find(".slider-selector-right").hide();
        }
        else{
            $sliderWrapper.find(".slider-selector-right").show();
        }
        if(sliderTarget <= 0){
            $sliderWrapper.find(".slider-selector-left").hide();
        }
        else{
            $sliderWrapper.find(".slider-selector-left").show();
        }

        var target = (sliderTarget) * width + (sliderTarget * 30);
        $(".slider").scrollLeft(target);

    }
});

window.getCookie = function(name) {
    var match = document.cookie.match(new RegExp('(^| )' + name + '=([^;]+)'));
    if (match)
        return match[2];
}

var Webflow = Webflow || [];
Webflow.push(function() {
    $('form').submit(function() {
        var formId = $(this).parent().data('formidhubspot');
        var url = "https://api.hsforms.com/submissions/v3/integration/submit/2629990/" + formId;
        var form = $(this);
        var data = {
            "fields": form.serializeArray(),
            "context": {
                "hutk": getCookie("hubspotutk"),
                "pageUri": window.location.href,
                "pageName": document.title
            },
            "legalConsentOptions": {
                "consent": {
                    "consentToProcess": true,
                    "text": "I agree to allow Example Company to store and process my personal data.",
                    "communications": [{
                        "value": true,
                        "subscriptionTypeId": 999,
                        "text": "I agree to receive marketing communications from Example Company."
                    }]
                }
            }
        };
        form.hide();
        form.parent().find('.loading-message').show().animate({
            width: "40px"
        }, 500);

        $.ajax({
            type: "POST",
            url: url,
            data: JSON.stringify(data),
            contentType: "application/json",
            dataType: "json",
            success: function(data) {
                form.hide();
                form.parent().find('.loading-message').hide();
                form.parent().find('.w-form-done').show();
            },
            error: function(data) {
                form.parent().find('.loading-message').hide();
                form.parent().find('.w-form-fail').show();
                var errortext;
                $.each(data.errors, function() {
                    errortext = errortext + data.message;
                });
                form.parent().find('.w-form-fail').html(errortext);
            }
        });
        return false;
    });
});
